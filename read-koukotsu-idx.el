(defvar shuowen-radicals (make-vector 512 nil))

(defun character-equal (char1 char2)
  (let (ret ic1 ic2)
    (if (consp char1)
	(if (setq ret (find-char char1))
	    (setq char1 ret)))
    (if (consp char2)
	(if (setq ret (find-char char2))
	    (setq char2 ret)))
    (or (eq char1 char2)
	(and (setq ic1 (get-char-attribute
			char1 'ideographic-combination))
	     (setq ic2 (get-char-attribute
			char2 'ideographic-combination))
	     (= (length ic1)(length ic2))
	     (every (lambda (e1 e2)
		      (equal (if (characterp e1)
				 e1
			       (or (find-char e1)
				   e1))
			     (if (characterp e2)
				 e2
			       (or (find-char e2)
				   e2))))
		    ic1 ic2))
	(and (setq ic1 (ideographic-structure-to-ids
			(get-char-attribute char1 'ideographic-structure)))
	     (setq ic2 (ideographic-structure-to-ids
			(get-char-attribute char2 'ideographic-structure)))
	     (string= ic1 ic2))
	)))

(with-current-buffer "koukotsu-idx.csv"
  (goto-char (point-min))
  (let (page
	sr-num sr-c num oc a-oc r-oc modern same same-type same-fn oids ois
	bunhen zoku-bunhen sources s-beg s-end i
	ret rest dest code ac-spec same-oc same-a-oc ic1 ic2)
    (while (re-search-forward "^\\([0-9]+\\),\\([0-9]+\\),\\([^,]+\\),\\([0-9]+\\),\\(.\\),\\([^,]+\\),\\([^,]+\\),\\([^,]+\\),\\([^,]+\\),\\([^,]+\\)," nil t)
      (setq page (match-string 1)
	    sr-num (match-string 2)
	    sr-c (match-string 3)
	    num (match-string 4)
	    oc (match-string 5)
	    modern (match-string 6)
	    same (match-string 7)
	    oids (match-string 8)
	    bunhen (match-string 9)
	    zoku-bunhen (match-string 10)
	    sources (buffer-substring (match-end 0)(point-at-eol)))
      (setq page (string-to-number page))
      (setq sr-num (string-to-number sr-num))
      (if (string-match "[ \t]+$" sr-c)
	  (setq sr-c (substring sr-c 0 (match-beginning 0))))
      (if (> sr-num 0)
	  (if (> (length sr-c) 0)
	      (aset shuowen-radicals (1- sr-num) (aref sr-c 0))
	    (setq sr-c nil))
	(setq sr-num nil))
      (if (string-match "[ \t]+$" modern)
	  (setq modern (substring modern 0 (match-beginning 0))))
      (if (= (length modern) 1)
	  (setq modern (aref modern 0)))
      (if (string-match "[ \t]+$" same)
	  (setq same (substring same 0 (match-beginning 0))))
      (setq same-type nil)
      (when (>= (length same) 1)
	(cond ((eq (aref same 0) ?$B"*(B)
	       (setq same (substring same 1))
	       )
	      ((eq (aref same 0) ?$B"M(B)
	       (setq same (substring same 1)
		     same-type 'zinbun)
	       ))
	(if (= (length same) 1)
	    (setq same (aref same 0))
	  ))
      (setq same
	    (cond ((characterp same)
		   (if (setq ret (char-ucs same))
		       (or (decode-char '=ucs ret)
			   same))
		   )
		  ((>= (length same) 2)
		   (setq dest nil
			 rest same)
		   (while (setq ret (ids-parse-element rest nil))
		     (setq dest (cons (car ret) dest))
		     (setq rest (cdr ret)))
		   (if (cdr dest)
		       (list
			(cons 'ideographic-combination
			      (nreverse dest)))
		     (car dest)))
		  ))
      (if (string-match "[ \t]+$" oids)
	  (setq oids (substring oids 0 (match-beginning 0))))
      (setq ois
	    (unless (string-match "\\?" oids)
	      (cdr (assq 'ideographic-structure
			 (ids-parse-string oids)))))
      (setq sources
	    (unless (string-match "^[ \t]*$" sources)
	      (split-string sources " ")))
      (while (progn
	       (forward-line)
	       (looking-at "^\\([0-9]+\\),[ \t]*,[ \t]*,[ \t]*,[ \t]*,[ \t]*,[ \t]*,[ \t]*,[ \t]*,[ \t]*,"))
	(setq ret (buffer-substring (match-end 0)(point-at-eol)))
	(unless (string-match "^[ \t]*$" ret)
	  (setq sources (append sources (split-string ret " ")))))
      (cond
       ((> (length num) 0)
	(setq num (string-to-number num))
	(setq oc (aref oc 0))
	(cond
	 ((eq (decode-char '=zinbun-oracle num) oc)
	  (put-char-attribute oc '=zinbun-oracle num)
	  (put-char-attribute oc 'zinbun-oracle-page page)
	  (if sr-num
	      (put-char-attribute oc 'shuowen-radical sr-num))
	  (cond
	   ((characterp modern)
	    (unless (char-feature oc '<-Oracle-Bones)
	      (if (setq ret (char-ucs modern))
		  (setq modern (or (decode-char '=ucs ret)
				   modern)))
	      (put-char-attribute oc '<-Oracle-Bones (list modern)))
	    )
	   ((string-match "=\\([0-9]+\\)" modern)
	    (setq code (string-to-int (match-string 1 modern)))
	    (setq r-oc (decode-char '=zinbun-oracle code))
	    (remove-char-attribute r-oc '<-denotational)
	    (remove-char-attribute oc '<-denotational)
	    (setq ac-spec
		  (intersection (char-attribute-alist r-oc)
				(char-attribute-alist oc)
				:test #'equal))
            (setq a-oc (define-char
			 (cons
			  (cons '=>zinbun-oracle code)
			  ac-spec)))

	    (unless (memq r-oc (setq ret (get-char-attribute
					  a-oc '->denotational)))
	      (put-char-attribute
	       a-oc '->denotational (append ret (list r-oc))))
	    (setq ret (get-char-attribute r-oc '<-denotational))
	    (unless (memq a-oc ret)
	      (put-char-attribute r-oc '<-denotational (cons a-oc ret)))

	    (unless (memq oc (setq ret (get-char-attribute
					a-oc '->denotational)))
	      (put-char-attribute
	       a-oc '->denotational (append ret (list oc))))
	    (setq ret (get-char-attribute oc '<-denotational))
	    (unless (memq a-oc ret)
	      (put-char-attribute oc '<-denotational (cons a-oc ret)))
	    )
	   ((>= (length modern) 2)
	    (unless (char-feature oc '<-Oracle-Bones)
	      (unless (string-match "[?$B!)(B]" modern)
		(setq dest nil
		      rest modern)
		(while (setq ret (ids-parse-element rest nil))
		  (setq dest (cons (car ret) dest))
		  (setq rest (cdr ret)))
		(put-char-attribute oc '<-Oracle-Bones
				    (if (cdr dest)
					(list
					 (list
					  (cons 'ideographic-combination
						(nreverse dest))))
				      dest)))
	      )
	    ))
	  (when same
	    (setq same-fn
		  (if same-type
		      (intern (format "<-Oracle-Bones@%s" same-type))
		    '<-Oracle-Bones))
	    (setq same-a-oc nil)
	    (when (characterp same)
	      (setq rest
		    (or (get-char-attribute
			 same
			 (if same-type
			     (intern (format "->Oracle-Bones@%s" same-type))
			   '->Oracle-Bones))
			(get-char-attribute same '->Oracle-Bones)))
	      (when (and rest
			 (setq same-oc (car rest))
			 (setq code (encode-char same-oc '=zinbun-oracle)))
		(unless (eq oc same-oc)
		  (setq ac-spec (intersection
				 (char-attribute-alist oc)
				 (char-attribute-alist same-oc)
				 :test #'equal))
		  (setq same-a-oc (define-char
				    (cons
				     (cons '=>zinbun-oracle code)
				     ac-spec)))
		  (setq ret (get-char-attribute same-a-oc '->denotational))
		  (unless (memq oc ret)
		    (setq ret (cons oc ret)))
		  (unless (memq same-oc ret)
		    (setq ret (cons same-oc ret)))
		  (put-char-attribute same-a-oc '->denotational ret)
                  ;; (setq ret (get-char-attribute same-a-oc '<-Oracle-Bones))
                  ;; (dolist (mc (get-char-attribute oc '<-Oracle-Bones))
                  ;;   (unless (or (memq mc ret)
                  ;;               (member* mc ret :test #'character-equal))
                  ;;     (setq ret (cons mc ret))))
                  ;; (dolist (mc (get-char-attribute same-oc '<-Oracle-Bones))
                  ;;   (unless (or (memq mc ret)
                  ;;               (member* mc ret :test #'character-equal))
                  ;;     (setq ret (cons mc ret))))
                  ;; (put-char-attribute same-a-oc '<-Oracle-Bones ret)
		  )))
	    (unless same-a-oc
	      (unless (member same (get-char-attribute oc same-fn))
		(put-char-attribute
		 oc
		 same-fn
		 (cons same (get-char-attribute oc same-fn)))))
	    )
          (if ois
	      (put-char-attribute oc 'ideographic-structure ois))
	  (when sources
	    (setq dest nil)
	    (while sources
	      (setq ret (pop sources))
	      (setq dest
		    (cons
		     (intern
		      (if (equal (car sources) "-")
			  (progn
			    (setq sources (cdr sources))
			    (format "zob1959=%s-%s" ret (pop sources)))
			(format "zob1959=%s" ret)))
		     dest)))
	    (put-char-attribute
	     oc 'sources
	     (nreverse dest)))
	  )
	 (t
	  (error "ooo")))
	)
       (same
	(setq oc (get-char-attribute same '->Oracle-Bones))
	)
       )
      )
    (map-char-attribute
     (lambda (char val)
       (setq dest (get-char-attribute char '<-Oracle-Bones))
       (dolist (c-oc (get-char-attribute char '->denotational))
	 (dolist (c-mc (char-feature c-oc '<-Oracle-Bones))
	   (unless (member* c-mc dest :test #'character-equal)
	     (setq dest (append dest (list c-mc))))))
       (put-char-attribute char '<-Oracle-Bones dest)
       nil)
     '=>zinbun-oracle)
    (map-char-attribute
     (lambda (char val)
       (dolist (c-oc (get-char-attribute char '->denotational))
	 (when (and (setq ic1 (get-char-attribute c-oc '<-Oracle-Bones))
		    (setq ic2 (get-char-attribute char '<-Oracle-Bones))
		    (= (length ic1)(length ic2))
		    (every #'character-equal ic1 ic2))
	   (remove-char-attribute c-oc '<-Oracle-Bones)))
       )
     '=>zinbun-oracle)
    ))
