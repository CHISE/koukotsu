# Koukotsu

A dataset of ”Catalogue of the Oracle Bones in the Kyoto University Research Institute for Humanistic Studies”（京都大学人文科学研究所所蔵甲骨文字索引） by Shigeki Kaizuka (貝塚茂樹) published in 1968 (https://chise.zinbun.kyoto-u.ac.jp/koukotsu/SAKUIN/) and related tools.