(let ((i 1)
      files)
  (while (<= i 3245)
    (setq files
	  (directory-files "/pub/koukotsu/rubbings/"
			   nil
			   (format "^[A-Z]%04d[a-z]*\\.png$" i)))
    (when files
      (with-temp-buffer
	(insert
	 (format
	  "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
	\"http://www.w3.org/TR/html4/loose.dtd\">
<html>
<head>
<title>%04d</title>
</head>

<body>

<h1>%04d</h1>

"
	  i i))
	(dolist (file files)
	  (insert
	   (format
	    "<img alt=\"%s\" src=\"/koukotsu/rubbings/%s\">\n"
	    file file)))
	(insert "
</body>
</html>
")
	(write-region (point-min)(point-max)
		      (expand-file-name
		       (format "%04d.html" i)
		       "/pub/koukotsu/rubbings/"))))
    (setq i (1+ i))))
